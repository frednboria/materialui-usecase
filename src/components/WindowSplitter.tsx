import * as React from "react";
import {useEffect, useRef} from "react";

export interface WindowSplitterProps {
  Left : React.ReactNode,
  Right : React.ReactNode
}

interface mouseDownInfoInterface {
  e : any,
  leftWidth : number,
  rightWidth : number,
  separatorLeft : number
}

export const WindowSplitter = ({Left, Right} : WindowSplitterProps) => {

  // create element ref
  const separatorRef = useRef(null);
  const leftRef = useRef(null);
  const rightRef = useRef(null);

  useEffect(() => {
    const divSeparator = separatorRef.current;
    const divLeft = leftRef.current;
    const divRight = rightRef.current;
    let mouseDownInfo : mouseDownInfoInterface;

    const onMouseDown = ( event : any ) => {
      document.addEventListener('mousemove', onMouseMove);
      document.addEventListener('mouseup', onMouseUp);
      mouseDownInfo = {
        e : event,
        leftWidth : divLeft.offsetWidth,
        rightWidth: divRight.offsetWidth,
        separatorLeft : divSeparator.offsetLeft
      }
    }
  
    const onMouseMove = (event : any) => {
      let delta = event.clientX - mouseDownInfo.e.x;
      divSeparator.style.left = `${mouseDownInfo.separatorLeft + delta}px`;
      divLeft.style.width = `${mouseDownInfo.leftWidth + delta}px`;
      divRight.style.width = `${mouseDownInfo.rightWidth - delta}px`;
    }
  
    const onMouseUp = (event:any) => {
      document.removeEventListener("mousemove", onMouseMove);
      document.removeEventListener("mouseup", onMouseUp);
    }
  
    // subscribe event
    divSeparator.addEventListener('mousedown', onMouseDown);
    
    return () => {
        // unsubscribe event
        divSeparator.removeEventListener("mousedown", onMouseDown);
        document.removeEventListener("mousemove", onMouseMove);
        document.removeEventListener("mouseup", onMouseUp);
    };
  }, []);



  return (
    <>
      <div className="split left" id="left" ref={leftRef} style={{width:"30%"}}>
        {Left}
      </div>
      
      <div className="split right" id="right" ref={rightRef} style={{width:"70%"}}>
        {Right}
      </div>
      
      <div className="split separator" id="separator" ref={separatorRef} style={{left:`${30}%`}}></div>
    </>
)};

