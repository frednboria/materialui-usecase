import * as React from "react";

export interface AvatarProps {
    src: string,
    name: string
}

export const Avatar = (props: AvatarProps) => (
    <div className="centered">
        <img src={props.src} alt="Avatar persona" />
        <h2>{props.name}</h2>
        <p>Some text.</p>
    </div>
)

