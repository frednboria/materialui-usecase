import * as React from "react";
import * as ReactDOM from "react-dom";

import { WindowSplitter } from "../components/WindowSplitter";
import {Avatar} from "../components/Avatar";
import { Hello } from "../components/Hello";


ReactDOM.render(
  <>
    <WindowSplitter 
      Left={<Hello compiler="TypeScript" framework="React" />}
      Right={<Avatar src={"../rsc/img/img_avatar.png"} name="John Doe" />}
    />
  </>,
  document.getElementById("container")
);