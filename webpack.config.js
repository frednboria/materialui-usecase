const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
  mode: "production",

  // Enable sourcemaps for debugging webpack's output.
  devtool: "source-map",

  resolve: {
    // Add '.ts' and '.tsx' as resolvable extensions.
    extensions: [ ".tsx"]
  },

  
  context: path.resolve(__dirname, 'src'),
  entry: './pages/index.tsx',

  module: {
    rules: [
      {
        test: /(?!.*.spec\.ts$).*\.ts(x?)$/,
        exclude: [
          /node_modules/,
          /cypress/,
          path.resolve(__dirname, 'cypress/integration'),
          /sample_spec\.ts/
        ],
        use: [
          {
            loader: "ts-loader"
          }
        ]
      },
      // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
      {
        enforce: "pre",
        test: /\.js$/,
        loader: "source-map-loader"
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader',
        ],
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },
    ]
  },

  // When importing a module whose path matches one of the following, just
  // assume a corresponding global variable exists and use that instead.
  // This is important because it allows us to avoid bundling all of our
  // dependencies, which allows browsers to cache those libraries between builds.
  externals: [
    /cypress/,
    {
      react: "React",
      "react-dom": "ReactDOM"
  }],

  plugins: [
    new HtmlWebPackPlugin({
      template: "../rsc/html/index.html",
      filename: "./index.html"
    })
  ]
};